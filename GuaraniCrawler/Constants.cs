﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace GuaraniCrawler
{
    public class Constants
    {
        /* Esta constante no se está usando todavía */
        public static Element ACT_PROVISORIA_FINALES = new Element()
        {
            href = "#Consultas",
            isChild = true,
            childNo = 3
        };

        // Configuración
        public static string URL = ConfigurationManager.AppSettings["URL"].ToString();
        public static bool ENABLE_CRAWL = Convert.ToBoolean(ConfigurationManager.AppSettings["ENABLE_CRAWL"].ToString());

        // Información del usuario
        public static string USER = ConfigurationManager.AppSettings["USER"].ToString();
        public static string PASSWORD = ConfigurationManager.AppSettings["PASSWORD"].ToString();
    }
}
