﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using mshtml;
using System.Threading;

namespace GuaraniCrawler
{
    public partial class Form1 : Form
    {
        private string username = Constants.USER;
        private string password = Constants.PASSWORD;
        
        public Form1()
        {
            InitializeComponent();
            if (!Constants.ENABLE_CRAWL)
            {
                return;
            }
            //MAIN Fetch
            this.webBrowser1.Url = new System.Uri(Constants.URL, System.UriKind.Absolute);
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_FirstDocumentCompleted);           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("HAY TODOs");
            //TODO Mostrar grilla para reemplazar el browser
            //TODO Poner un timer, configurable
            //TODO Otras pantallas

        }

        private void loadLoginPage()
        {
            webBrowser1.DocumentCompleted -= webBrowser1_FirstDocumentCompleted;

            HtmlWindowCollection frames = webBrowser1.Document.Window.Frames;

            foreach (HtmlWindow frame in frames)
            {
                HtmlElement elem = frame.Document.GetElementById("id_barra_botones");
                if (elem != null)
                {
                    HtmlElementCollection links = elem.GetElementsByTagName("A");
                    if (links.Count > 0)
                    {
                        foreach (HtmlElement link in links)
                        {
                            if (link.GetAttribute("HREF").Contains("identi"))
                            {
                                link.InvokeMember("Click");                                
                                webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_SecondDocumentCompleted);
                                break;
                            }
                            
                        }
                        
                    }
                }
            }   
        }

        private void fillUserData()
        {
            webBrowser1.DocumentCompleted -= webBrowser1_SecondDocumentCompleted;
            HtmlWindowCollection frames = webBrowser1.Document.Window.Frames;            
            foreach (HtmlWindow frame in frames)
            {                
                HtmlElementCollection elementos = frame.Document.GetElementsByTagName("input");
                if (elementos.Count > 0)
                {   
                    HtmlElement passwordInput = null;
                    HtmlElement acceptButton = null;
                    foreach (HtmlElement elem in elementos)
                    {
                        if (elem.GetAttribute("name").Contains("fUsuario"))
                        {
                            elem.SetAttribute("value", this.username);
                        }
                        if (elem.GetAttribute("name").Contains("bClave"))
                        {
                            //Console.WriteLine(this.password);
                            passwordInput = elem;
                            elem.SetAttribute("value", this.password);
                        }
                        if(elem.GetAttribute("name").Contains("Aceptar"))
                        {
                            acceptButton = elem;
                        }
                    }

                    if (passwordInput == null || acceptButton == null)
                    {
                        Console.Error.WriteLine("Hay inconsistencias en la estructura HTML, no se pudo ubicar el Password Input o botón Aceptar (Login).");
                        return;
                    }
                        
                    //Espera
                    while (string.IsNullOrEmpty(passwordInput.GetAttribute("value"))) // 2014-12-16 No está cargando el password sin esperar
                    {
                        System.Threading.Thread.Sleep(100);
                    }      
             
                    
                    acceptButton.InvokeMember("Click");  
                    webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_ThirdDocumentCompleted);
                       
                }                
               
            }
        }

        private void buscarProvisoriaFinales()
        {
            webBrowser1.DocumentCompleted -= webBrowser1_ThirdDocumentCompleted;
            HtmlWindowCollection frames = webBrowser1.Document.Window.Frames;
            foreach (HtmlWindow frame in frames)
            {
                HtmlElement sidebar = frame.Document.GetElementById("mainMenu");

                if (sidebar != null)
                {
                    Console.WriteLine("HAY SIDEBAR");
                    foreach (HtmlElement elem in sidebar.GetElementsByTagName("a"))
                    {
                        if(elem.GetAttribute("href").Contains("consultarActProvisoria.php")){
                            elem.InvokeMember("Click");                            
                        }
                    }
                }                
                
            }

            //HtmlWindowCollection frames = webBrowser1.Document.Window.Frames;
            //foreach (HtmlWindow frame in frames)
            //{
            //    HtmlElementCollection elementos = frame.Document.GetElementsByTagName("a");
            //    if (elementos.Count > 0)
            //    {
            //        foreach (HtmlElement elem in elementos)
            //        {
            //            Console.WriteLine(elem.GetAttribute("href"));
            //            if (elem.GetAttribute("href").Contains("consultarActProvisoria.php"))
            //            {
            //                elem.InvokeMember("Click");
            //            }
            //        }
                    
            //    }

            //}
        }

        private void webBrowser1_FirstDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url != webBrowser1.Url)
            {
                return;
            }
            
            loadLoginPage();
            
        }

        private void webBrowser1_SecondDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            fillUserData();
        }

        private void webBrowser1_ThirdDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url != webBrowser1.Url)
            {
                return;
            }
            buscarProvisoriaFinales();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

      
    }
}
